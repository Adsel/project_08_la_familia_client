package pl.kappserv;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.kappserv.gui.ScreenController;

import java.io.IOException;

public class Main extends Application {
    private Parent root;
    private Scene myScene;
    private Stage primaryStage;
    private static ScreenController screenController;

    public static ScreenController getSC() {
        return screenController;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        root = FXMLLoader.load(getClass().getResource("stages/myMain.fxml"));
        myScene = new Scene(root, 1355, 750);
        myScene.getStylesheets().add(getClass().getResource("stages/style.css").toExternalForm());
        screenController = new ScreenController(myScene, primaryStage);
        reloadScreen();
        primaryStage.setMinHeight(720);
        primaryStage.setMinWidth(1420);
        primaryStage.setMaxHeight(720);
        primaryStage.setMaxWidth(1420);
        primaryStage.setTitle("Pizzeria La Familia");
        primaryStage.setScene(myScene);
        primaryStage.show();
    }

    public static void activeScreen(String screenName) {
        screenController.activate(screenName);
    }
    public static void activeScreen(String screenName, int x, int y){
        screenController.activate( screenName,x,y );
    }

    public static void reloadScreen() throws IOException {
        screenController.removeAllScreen();
        screenController.addScreen("serverDisconnect",FXMLLoader.load(Main.class.getResource("stages/noConnectionController.fxml")));
        screenController.addScreen("myMain", FXMLLoader.load(Main.class.getResource( "stages/myMain.fxml" )));
        screenController.addScreen("myMenu", FXMLLoader.load(Main.class.getResource( "stages/myMenu.fxml" )));
        screenController.addScreen("myOrder", FXMLLoader.load(Main.class.getResource( "stages/myOrder.fxml" )));
        screenController.addScreen("pizzaOptions", FXMLLoader.load(Main.class.getResource("stages/pizzaOptions.fxml")));
    }

    public static void drawGUI(String[] args) {
        launch(args);
    }

    public static void goNoConnection(String msg){
        System.out.println(msg);
        activeScreen("serverDisconnect", 480,480);
    }
}