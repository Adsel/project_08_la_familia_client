package pl.kappserv.utils.conventers;

import pl.kappserv.model.Pizza;
import pl.kappserv.modelFx.PizzaFx;

public class ConverterPizza {
    public static PizzaFx convertToPizzaFx(Pizza pizza){
        PizzaFx pizzaFx = new PizzaFx();
        pizzaFx.setId(pizza.getId());
        pizzaFx.setName(pizza.getName());
        pizzaFx.setPrice(pizza.getPrice());
        pizzaFx.setCode(pizza.getCode());
        pizzaFx.setComposition(pizza.getComposition());
        pizzaFx.setSize(pizza.getSize());
        pizzaFx.setDesc(pizza.getDesc());

        return pizzaFx;
    }
    //TODO
    public static Pizza convertToPizza(PizzaFx pizzaFx){
        Pizza pizza = new Pizza();
        pizza.setId(Integer.valueOf(pizzaFx.getId().get()));
        pizza.setName(pizzaFx.getName().get());
        pizza.setPrice(Integer.valueOf(pizzaFx.getPrice().get()));
        pizza.setCode(pizzaFx.getCode().get());
        pizza.setComposition(pizzaFx.getComposition().get());
        pizzaFx.setDesc(pizzaFx.getDesc().get());

        return pizza;
    }
}
