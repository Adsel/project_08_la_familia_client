package pl.kappserv.utils.conventers;

import pl.kappserv.model.Drink;
import pl.kappserv.modelFx.DrinkFx;

public class ConverterDrink {
    public static DrinkFx convertToDrinkFx(Drink drink){
        DrinkFx drinkFx = new DrinkFx();
        drinkFx.setName(drink.getName());
        drinkFx.setPrice(drink.getPrice());
        drinkFx.setCode(drink.getCode());
        drinkFx.setVolume(drink.getVolume());
        drinkFx.setDesc(drink.getDesc());

        return drinkFx;
    }

    public static Drink convertToDrink(DrinkFx drinkFx){
        Drink drink = new Drink();
        drink.setName(drinkFx.getName().get());
        drink.setPrice(Integer.valueOf(drinkFx.getPrice().get()));
        drink.setCode(drinkFx.getCode().get());
        drink.setVolume(drinkFx.getVolume().get());
        drinkFx.setDesc(drinkFx.getDesc().get());

        return drink;
    }
}
