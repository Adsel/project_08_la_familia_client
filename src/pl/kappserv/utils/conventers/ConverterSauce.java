package pl.kappserv.utils.conventers;

import pl.kappserv.model.Sauce;
import pl.kappserv.modelFx.SauceFx;

public class ConverterSauce {
    public static SauceFx convertToSauceFx(Sauce sauce){
        SauceFx sauceFx = new SauceFx();
        sauceFx.setName(sauce.getName());
        sauceFx.setPrice(sauce.getPrice());
        sauceFx.setCode(sauce.getCode());
        sauceFx.setDesc(sauce.getDesc());

        return sauceFx;
    }

    public static Sauce convertToSauce(SauceFx sauceFx){
        Sauce sauce = new Sauce();
        sauce.setName(sauceFx.getName().get());
        sauce.setPrice(Integer.valueOf(sauceFx.getPrice().get()));
        sauce.setCode(sauceFx.getCode().get());
        sauce.setDesc(sauceFx.getDesc().get());

        return sauce;
    }
}
