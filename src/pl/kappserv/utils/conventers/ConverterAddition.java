package pl.kappserv.utils.conventers;

import javafx.scene.control.CheckBox;
import pl.kappserv.model.Addition;
import pl.kappserv.modelFx.AdditionFx;

public class ConverterAddition {
    public static AdditionFx convertToAdditionFx(Addition Addition){
        CheckBox x = new CheckBox();
        x.setSelected(false);
        AdditionFx AdditionFx = new AdditionFx();
        AdditionFx.setName(Addition.getName());
        AdditionFx.setPrice(Addition.getPrice());
        AdditionFx.setCode(Addition.getCode());
        AdditionFx.setDesc(Addition.getDesc());
        AdditionFx.setRemark(x);

        return AdditionFx;
    }

    public static Addition convertToAddition(AdditionFx AdditionFx){
        Addition Addition = new Addition();
        Addition.setName(AdditionFx.getName().get());
        Addition.setPrice(Integer.valueOf(AdditionFx.getPrice().get()));
        Addition.setCode(AdditionFx.getCode().get());
        AdditionFx.setDesc(AdditionFx.getDesc().get());

        return Addition;
    }
}
