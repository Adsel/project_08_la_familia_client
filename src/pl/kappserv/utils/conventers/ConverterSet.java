package pl.kappserv.utils.conventers;

import pl.kappserv.model.Set;
import pl.kappserv.modelFx.SetFx;

public class ConverterSet {
    public static SetFx convertToSetFx(Set set){
        SetFx setFx = new SetFx();
        setFx.setName(set.getName());
        setFx.setId(set.getId());
        setFx.setPrice(set.getPrice());
        setFx.setCode(set.getCode());
        setFx.setDesc(set.getDesc());

        return setFx;
    }

    public static Set convertToSet(SetFx setFx){
        Set set = new Set();
        set.setId(Integer.valueOf(setFx.getId().get()));
        set.setName(setFx.getName().get());
        set.setPrice(Integer.valueOf(setFx.getPrice().get()));
        set.setCode(setFx.getCode().get());
        set.setDesc(setFx.getDesc().get());

        return set;
    }
}
