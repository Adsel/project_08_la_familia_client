package pl.kappserv.utils.conventers;

import pl.kappserv.modelFx.BasketFx;

public class ConverterToBasket {
    public static BasketFx convertToBasketFx(Integer price, String name,  String code){
        BasketFx basketFx = new BasketFx();
        basketFx.setName(name);
        basketFx.setPrice(price);
        basketFx.setCode(code);
        return basketFx;
    }

}
