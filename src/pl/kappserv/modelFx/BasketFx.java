package pl.kappserv.modelFx;

import javafx.beans.property.SimpleStringProperty;

public class BasketFx {
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty price = new SimpleStringProperty();
    private SimpleStringProperty code = new SimpleStringProperty();

    public SimpleStringProperty getName(){ return name; }
    public SimpleStringProperty getPrice(){ return price; }
    public SimpleStringProperty getCode(){ return code; }


    public void setName(String name){ this.name.set(name); }
    public void setPrice(Integer price){ this.price.set(String.valueOf(((Double.valueOf(price))/100))); }
    public void setCode(String code){ this.code.set(code); }

    @Override
    public String toString() {
        String s = "BasketFX{" +
                ", name=" + name.get() +
                ", price=" + price.get() +
                ", code=" + code.get() +
                '}';
        return s;
    }
}
