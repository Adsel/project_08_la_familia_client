package pl.kappserv.modelFx;

import javafx.beans.property.SimpleStringProperty;
import pl.kappserv.gui.Controller;
import pl.kappserv.model.Sauce;

import java.util.ArrayList;

public class CompletedPizzaFx {
    private SimpleStringProperty id = new SimpleStringProperty();
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty price = new SimpleStringProperty();
    private SimpleStringProperty desc = new SimpleStringProperty();
    private SimpleStringProperty code = new SimpleStringProperty();
    private SimpleStringProperty size = new SimpleStringProperty();
    private SimpleStringProperty composition = new SimpleStringProperty();
    private ArrayList<SimpleStringProperty>  additions = new ArrayList<SimpleStringProperty>();


    public SimpleStringProperty getId(){ return id; }
    public SimpleStringProperty getName(){ return name; }
    public SimpleStringProperty getPrice(){ return price; }
    public SimpleStringProperty getDesc(){ return desc; }
    public SimpleStringProperty getCode(){ return code; }
    public SimpleStringProperty getSize(){ return size; }
    public SimpleStringProperty getComposition(){ return composition; }
    public SimpleStringProperty getAddition(int index){ return additions.get(index);}


    public void setId(Integer id){ this.id.set(id.toString()); }
    public void setName(String name){ this.name.set(name); }
    public void setPrice(Integer price){ this.price.set(Controller.convertFromGrString(price.toString())); }
    public void setDesc(String desc){ this.desc.set(desc); }
    public void setCode(String code){ this.code.set(code); }
    public void setSize(String size){this.size.set(size);}
    public void setComposition(String composition){ this.composition.set(composition); }


    @Override
    public String toString() {
        String s = "PizzaFx{" +
                "id=" + id.get() +
                ", name=" + name.get() +
                ", price=" + price.get() +
                ", desc=" + desc.get() +
                ", code=" + code.get() +
                ", size=" + size.get() +
                ", composition=" + composition.get() +
                '}';
        return s;
    }
}
