package pl.kappserv.modelFx;

import javafx.beans.property.SimpleStringProperty;

public class SetFx {
    private SimpleStringProperty id = new SimpleStringProperty();
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty price = new SimpleStringProperty();
    private SimpleStringProperty desc = new SimpleStringProperty();
    private SimpleStringProperty code = new SimpleStringProperty();

    public SimpleStringProperty getId(){ return id; }
    public SimpleStringProperty getName(){ return name; }
    public SimpleStringProperty getPrice(){ return price; }
    public SimpleStringProperty getDesc(){ return desc; }
    public SimpleStringProperty getCode(){ return code; }

    public void setId(Integer id){ this.id.set(id.toString()); }
    public void setName(String name){ this.name.set(name); }
    public void setPrice(Integer price){ this.price.set(price.toString()); }
    public void setDesc(String desc){ this.desc.set(desc); }
    public void setCode(String code){ this.code.set(code); }

    @Override
    public String toString() {
        String s = "SetFX{" +
                "id=" + id.get() +
                ", name=" + name.get() +
                ", price=" + price.get() +
                ", desc=" + desc.get() +
                ", code=" + code.get() +
                '}';
        return s;
    }
}
