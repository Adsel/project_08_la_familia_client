package pl.kappserv.modelFx;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.kappserv.gui.ConnectionRefusedException;
import pl.kappserv.gui.Controller;
import pl.kappserv.model.Set;
import pl.kappserv.utils.conventers.ConverterSet;

import java.io.IOException;
import java.util.List;

public class ListSetModel {
    private ObservableList<SetFx> setFxObservableList = FXCollections.observableArrayList();

    public void init() throws ConnectionRefusedException {
        try {
            List<Set> sets = Controller.request.getSetsFromServer();
            sets.forEach(set -> {
                this.setFxObservableList.add(ConverterSet.convertToSetFx(set));
            });
        } catch (IOException e) {
           throw new ConnectionRefusedException();
        }
    }

    public ObservableList<SetFx> getSetFxObservableList() {
        return this.setFxObservableList;
    }
}