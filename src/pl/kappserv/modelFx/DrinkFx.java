package pl.kappserv.modelFx;

import javafx.beans.property.SimpleStringProperty;

public class DrinkFx {
    private SimpleStringProperty id = new SimpleStringProperty();
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty price = new SimpleStringProperty();
    private SimpleStringProperty desc = new SimpleStringProperty();
    private SimpleStringProperty code = new SimpleStringProperty();
    private SimpleStringProperty volume = new SimpleStringProperty();

    public SimpleStringProperty getId(){ return id; }
    public SimpleStringProperty getName(){ return name; }
    public SimpleStringProperty getPrice(){ return price; }
    public SimpleStringProperty getDesc(){ return desc; }
    public SimpleStringProperty getCode(){ return code; }
    public SimpleStringProperty getVolume(){ return volume; }

    public void setId(Integer id){ this.id.set(id.toString()); }
    public void setName(String name){ this.name.set(name); }
    public void setPrice(Integer price){ this.price.set(price.toString()); }
    public void setDesc(String desc){ this.desc.set(desc); }
    public void setCode(String code){ this.code.set(code); }
    public void setVolume(String volume){ this.volume.set(volume); }

    @Override
    public String toString() {
        String s = "DrinkFX{" +
                "id=" + id.get() +
                ", name=" + name.get() +
                ", price=" + price.get() +
                ", desc=" + desc.get() +
                ", code=" + code.get() +
                ", volumes=" + volume.get() +
                '}';
        return s;
    }
}
