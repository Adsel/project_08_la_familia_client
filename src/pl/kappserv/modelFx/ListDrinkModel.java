package pl.kappserv.modelFx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.kappserv.gui.ConnectionRefusedException;
import pl.kappserv.gui.Controller;
import pl.kappserv.model.Drink;
import pl.kappserv.utils.conventers.ConverterDrink;

import java.io.IOException;
import java.util.List;


public class ListDrinkModel {
    private ObservableList<DrinkFx> drinkFxObservableList = FXCollections.observableArrayList();

    public void init() throws ConnectionRefusedException {
        try {
            List<Drink> drinks = Controller.request.getDrinksFromServer();
            drinks.forEach(drink -> {
                this.drinkFxObservableList.add(ConverterDrink.convertToDrinkFx(drink));
            });
        } catch (IOException e) {
            throw new ConnectionRefusedException();
        }
    }

    public ObservableList<DrinkFx> getDrinkFxObservableList() {
        return this.drinkFxObservableList;
    }
}