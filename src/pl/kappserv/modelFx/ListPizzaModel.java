package pl.kappserv.modelFx;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.kappserv.gui.ConnectionRefusedException;
import pl.kappserv.gui.Controller;
import pl.kappserv.model.Pizza;
import pl.kappserv.utils.conventers.ConverterPizza;

import java.io.IOException;
import java.util.List;

public class ListPizzaModel  {
    private ObservableList<PizzaFx> pizzaFxObservableList = FXCollections.observableArrayList();

    public void init() throws ConnectionRefusedException {
        try {
            List<Pizza> pizzas = Controller.request.getPizzasFromServer();
            pizzas.forEach(pizza -> {
                this.pizzaFxObservableList.add(ConverterPizza.convertToPizzaFx(pizza));
            });
        }
        catch (ConnectionRefusedException cre){
            throw new ConnectionRefusedException(cre.getMessage());
        }
        catch (IOException e) {
            throw new ConnectionRefusedException();
        }
    }

    public ObservableList<PizzaFx> getPizzaFxObservableList() {
        return this.pizzaFxObservableList;
    }
}