package pl.kappserv.modelFx;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.kappserv.gui.ConnectionRefusedException;
import pl.kappserv.gui.Controller;
import pl.kappserv.model.Sauce;
import pl.kappserv.utils.conventers.ConverterSauce;

import java.io.IOException;
import java.util.List;

public class ListSauceModel {
    private ObservableList<SauceFx> sauceFxObservableList = FXCollections.observableArrayList();

    public void init() throws ConnectionRefusedException {
        try {
            final List<Sauce> sauces = Controller.request.getSaucesFromServer();
            sauces.forEach(sauce -> {
                this.sauceFxObservableList.add(ConverterSauce.convertToSauceFx(sauce));
            });
        } catch (IOException e) {
            throw new ConnectionRefusedException();
        }
    }

    public ObservableList<SauceFx> getSauceFxObservableList() {
        return this.sauceFxObservableList;
    }
}