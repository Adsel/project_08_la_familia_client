package pl.kappserv.modelFx;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.kappserv.gui.ConnectionRefusedException;
import pl.kappserv.gui.Controller;
import pl.kappserv.model.Addition;
import pl.kappserv.utils.conventers.ConverterAddition;

import java.io.IOException;
import java.util.List;

public class ListAdditionModel {
    private ObservableList<AdditionFx> additionFxObservableList = FXCollections.observableArrayList();

    public void init() throws ConnectionRefusedException{
        try {
            final List<Addition> additions = Controller.request.getAdditionsFromServer();
            additions.forEach(addition -> {
                this.additionFxObservableList.add(ConverterAddition.convertToAdditionFx(addition));
            });
        } catch (IOException e) {
            throw new ConnectionRefusedException();
        }
    }

    public ObservableList<AdditionFx> getAdditionFxObservableList() {
        return this.additionFxObservableList;
    }
}