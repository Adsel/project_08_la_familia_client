package pl.kappserv.modelFx;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.CheckBox;

public class AdditionFx {
    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty price = new SimpleStringProperty();
    private SimpleStringProperty desc = new SimpleStringProperty();
    private SimpleStringProperty code = new SimpleStringProperty();
    private SimpleBooleanProperty selected = new SimpleBooleanProperty();
    private CheckBox remark = new CheckBox();

    public SimpleIntegerProperty getId(){ return id; }
    public SimpleStringProperty getName(){ return name; }
    public SimpleStringProperty getPrice(){ return price; }
    public SimpleStringProperty getDesc(){ return desc; }
    public SimpleStringProperty getCode(){ return code; }
    public CheckBox getRemark() { return remark; }
    public SimpleBooleanProperty getSelected(){ return selected;}

    public void setId(Integer id){ this.id.set(id); }
    public void setName(String name){ this.name.set(name); }
    public void setPrice(Integer price){ this.price.set(price.toString()); }
    public void setDesc(String desc){ this.desc.set(desc); }
    public void setCode(String code){ this.code.set(code); }
    public void setRemark(CheckBox remark) { this.remark = remark; }
    public void setSelected(Boolean selected) {this.selected.set(selected);}

    @Override
    public String toString() {
        String s = "AdditionFx{" +
                "id=" + id.get() +
                ", name=" + name.get() +
                ", price=" + price.get() +
                ", desc=" + desc.get() +
                ", code=" + code.get() +
                '}';
        return s;
    }
}
