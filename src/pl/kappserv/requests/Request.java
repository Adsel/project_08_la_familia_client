package pl.kappserv.requests;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import pl.kappserv.gui.Controller;
import pl.kappserv.model.*;
import pl.kappserv.gui.ConnectionRefusedException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Request {
    private final CloseableHttpClient client;
    private Gson gson;

    public Request(){
        client = HttpClientBuilder.create().build();
        gson = new Gson();
    }

    public static void createReceiptHTTPRequest(Receipt newReceipt) {
        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/api/files/create-file");
        Gson gson = new Gson();
        final Receipt userData = newReceipt;
        final String json = gson.toJson(userData);

        try {
            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            final CloseableHttpResponse response = client.execute(httpPost);

            if(response.getStatusLine().getStatusCode() == 404) {
                System.out.println("Prosze poprawic w kontrolerze sciezke do pliku - sciezka jest nieprawidlowa!");
            } else if(response.getStatusLine().getStatusCode() == 201) {
                final HttpEntity httpEntity = response.getEntity();
                final String jsonAsFileMetaData = EntityUtils.toString(httpEntity);
                final FileMetaData fileMetaData = gson.fromJson(jsonAsFileMetaData, FileMetaData.class);

                System.out.println("Dane zapisanego pliku:");
                System.out.printf("Data utworzenia pliku: %s \n", fileMetaData.getCreationDate());
                System.out.printf("Nazwa pliku: %s \n", fileMetaData.getFileName());
                System.out.printf("Rozmiar pliku: %d B \n", fileMetaData.getSize());
            }
            client.close();
        } catch (UnsupportedEncodingException e) {
            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }
    }

    public Pizza getPizzaFromServer(Integer id) throws IOException {
        final HttpGet request = new HttpGet("http://"+ Controller.getServerIp() + "/api/products/get-pizza-" + id);

        try {
            final HttpResponse response = client.execute(request);
            final HttpEntity entity = response.getEntity();
            final String json = EntityUtils.toString(entity);
            final Type type = new TypeToken<Pizza>(){}.getType();
            final Pizza pizza = gson.fromJson(json, type);

            return pizza;
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    public Drink getDrinkFromServer(Integer id) throws  IOException{
        final HttpGet request = new HttpGet("http://"+ Controller.getServerIp() + "/api/products/get-drink-" + id);
        try {
            final HttpResponse response = client.execute(request);
            final HttpEntity entity = response.getEntity();
            final String json = EntityUtils.toString(entity);
            final Type type = new TypeToken<Drink>(){}.getType();
            final Drink drink = gson.fromJson(json, type);

            return drink;
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    public Set getSetFromServer(Integer id) throws  IOException{
        final HttpGet request = new HttpGet("http://"+ Controller.getServerIp() + "/api/products/get-set-" + id);
        try {
            final HttpResponse response = client.execute(request);
            System.out.println(response.getStatusLine().getStatusCode() + " KOD");
            final HttpEntity entity = response.getEntity();
            final String json = EntityUtils.toString(entity);
            final Type type = new TypeToken<Set>(){}.getType();
            final Set set = gson.fromJson(json, type);

            return set;
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    public List<Drink> getDrinksFromServer() throws IOException {
        final HttpGet request = new HttpGet("http://"+ Controller.getServerIp() + "/api/products/get-drinks");

        try {
            final HttpResponse response = client.execute(request);
            final HttpEntity entity = response.getEntity();
            final String json = EntityUtils.toString(entity);
            final Type type = new TypeToken<ArrayList<Drink>>(){}.getType();
            final List<Drink> drinks = gson.fromJson(json, type);

            return drinks;
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    public List<Pizza> getPizzasFromServer() throws IOException, ConnectionRefusedException {
        final HttpGet request = new HttpGet("http://"+ Controller.getServerIp() + "/api/products/get-pizzas");
        HttpResponse response = null;
        try {
            response = client.execute(request);
            final HttpEntity entity = response.getEntity();
            final String json = EntityUtils.toString(entity);
            final Type type = new TypeToken<ArrayList<Pizza>>(){}.getType();
            final List<Pizza> pizzas = gson.fromJson(json, type);

            return pizzas;
        } catch (IOException e) {
            if(response == null){
                throw new ConnectionRefusedException("Zawiodla proba polaczenia z serwerem");
            }
            throw new IOException(e.getMessage());
        }
    }

    public List<Addition> getAdditionsFromServer() throws IOException {
        final HttpGet request = new HttpGet("http://"+ Controller.getServerIp() + "/api/products/get-additions");

        try {
            final HttpResponse response = client.execute(request);
            final HttpEntity entity = response.getEntity();
            final String json = EntityUtils.toString(entity);
            final Type type = new TypeToken<ArrayList<Addition>>(){}.getType();
            final List<Addition> adds = gson.fromJson(json, type);

            return adds;
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    public List<Sauce> getSaucesFromServer() throws IOException {
        final HttpGet request = new HttpGet("http://"+ Controller.getServerIp() + "/api/products/get-sauces");

        try {
            final HttpResponse response = client.execute(request);
            final HttpEntity entity = response.getEntity();
            final String json = EntityUtils.toString(entity);
            final Type type = new TypeToken<ArrayList<Sauce>>(){}.getType();
            final List<Sauce> sauces = gson.fromJson(json, type);

            return sauces;
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    public List<Set> getSetsFromServer() throws IOException {
        final HttpGet request = new HttpGet("http://"+ Controller.getServerIp() + "/api/products/get-sets");

        try {
            final HttpResponse response = client.execute(request);
            final HttpEntity entity = response.getEntity();
            final String json = EntityUtils.toString(entity);
            final Type type = new TypeToken<ArrayList<Set>>(){}.getType();
            final List<Set> sets = gson.fromJson(json, type);

            return sets;
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }
}
