package pl.kappserv.model;

public class Pizza extends Product {
        private String size;
        private String composition;

        public Pizza(){ super(); }

        public Pizza(Integer id, Integer price, String name, String desc, String code, String size, String composition){
            super(id, price, name, desc, code);
            this.size = size;
            this.composition = composition;
        }

        public void setSize(String size) { this.size = size; }

        public void setComposition(String composition) {
        this.composition = composition;
    }

        public String getSize() {
        return size;
    }

        public String getComposition() { return composition; }
}
