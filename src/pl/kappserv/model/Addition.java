package pl.kappserv.model;

public class Addition extends Product {
    public Addition() {
        super();
    }

    public Addition(Integer id, Integer price, String name, String desc, String code) {
        super(id, price, name, desc, code);
    }
}