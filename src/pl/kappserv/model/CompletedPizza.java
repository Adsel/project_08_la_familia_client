package pl.kappserv.model;

import pl.kappserv.modelFx.ListPizzaModel;
import pl.kappserv.modelFx.PizzaFx;

import java.util.ArrayList;


public class CompletedPizza extends Pizza {
    PizzaFx pizzaFx;
    ListPizzaModel listPizzaModel;
    private ArrayList<Addition> additions;
    private ArrayList<Sauce> sauces;
    private Boolean whole;

    public CompletedPizza(){
        super();
    }

    public CompletedPizza(Integer id, Integer price, String name, String desc, String code,  String size, String composition, ArrayList<Addition> additions, ArrayList<Sauce> sauces, Boolean whole){
        super(id, price, name, desc, code, size , composition);
        this.additions = additions;
        this.sauces = sauces;
        this.whole = whole;
    }

    public ArrayList<Addition> getAdditions() {
        return additions;
    }

    public ArrayList<Sauce> getSauces() {
        return sauces;
    }

    @Override
    public Integer getPrice() {
        Integer p = super.getPrice();
        for(Addition a: additions){
            p += a.getPrice();
        }

        for(Sauce s: sauces){
            p += s.getPrice();
        }

        if(this.whole){
            return p;
        }
        else{
            return Math.round(p / 2);
        }
    }

    @Override
    public String toString(){
        String result =
                "id=" + this.getId() +
                        ",name=" + this.getName() +
                        ",price=" + this.getPrice() +
                        ",desc=" + this.getDesc() +
                        ",code=" + this.getCode() +
                        ",composition=" + this.getComposition() +
                        ",additions=[";
        for(Addition a: this.additions){
            result += ",name=" + a.getName();
        }
        result += "],sauces=[";

        for(Sauce s: this.sauces){
            result += ",name=" + s.getName();
        }
        result +="], whole=" + this.whole;

        return result;
    }
}
