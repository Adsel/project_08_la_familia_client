package pl.kappserv.model;

import java.util.ArrayList;

public class Receipt {
    private Integer price;
    private ArrayList<Drink> drinks;
    private ArrayList<CompletedSet> sets;
    private ArrayList<CompletedPizza> pizzas;

    public Receipt(ArrayList<Drink> drinks, ArrayList<CompletedSet> sets, ArrayList<CompletedPizza> pizzas){
        this.drinks = drinks;
        this.sets = sets;
        this.pizzas = pizzas;
        setPrice();
    }

    public Receipt(){
        price = 0;
    }

    private void setPrice(){
        Integer p = 0;
        for(CompletedPizza cp: pizzas){
            p += cp.getPrice();
        }

        for(Drink cd: drinks){
            p += cd.getPrice();
        }

        for(CompletedSet cs: sets){
            p += cs.getPrice();
        }

        this.price = p;
    }

    public Integer getPrice(){
        return this.price;
    }
}
