package pl.kappserv.model;

public class Drink extends Product {
        private String volume;

        public Drink(){
            super();
        }

        public Drink(Integer id, Integer price, String name, String desc, String code, String volume){
            super(id, price, name, desc, code);
            this.volume = volume;
        }

        public String getVolume () {
            return volume;
        }

        public void setVolume ( String volume) { this.volume = volume; }
}