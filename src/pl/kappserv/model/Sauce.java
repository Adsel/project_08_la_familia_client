package pl.kappserv.model;

public class Sauce extends Product {
    public Sauce(){
        super();
    }

    public Sauce(Integer id, Integer price, String name, String desc, String code){
        super(id, price, name, desc, code);
    }
}