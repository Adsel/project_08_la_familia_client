package pl.kappserv.model;

public class Product {
    private Integer id;
    private Integer price;
    private String name;
    private String desc;
    private String code;

    public Product(){ }

    public Product(Integer id, Integer price, String name, String desc, String code){
        this.id = id;
        this.price = price;
        this.name = name;
        this.desc = desc;
        this.code = code;
    }

    public Integer getPrice(){
        return this.price;
    }
    public String getName(){
        return this.name;
    }
    public String getDesc() {
        return desc;
    }
    public String getCode() {
        return code;
    }
    public Integer getId(){ return id; }

    public void setId(Integer id){ this.id = id; }
    public void setPrice(Integer price){ this.price = price; }
    public void setName(String name){ this.name = name; }
    public void setDesc(String name){ this.name = name; }
    public void setCode(String code){ this.code = code; }
}
