package pl.kappserv.model;

public class Promotion {
    private String name;
    private String description;
    private String requirements;

    public Promotion(){ }

    public Promotion(String name, String description, String requirements){
        this.name = name;
        this.description = description;
        this.requirements = requirements;
    }
}