package pl.kappserv.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.kappserv.gui.ConnectionRefusedException;
import pl.kappserv.gui.Controller;
import pl.kappserv.gui.MyOrderController;
import pl.kappserv.modelFx.BasketFx;
import pl.kappserv.modelFx.DrinkFx;
import pl.kappserv.utils.conventers.ConverterDrink;
import pl.kappserv.utils.conventers.ConverterToBasket;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Basket implements Serializable {
    private static ArrayList<CompletedPizza> pizzas = new ArrayList<>();
    private static ArrayList<Drink> drinks = new ArrayList<>();
    private static ArrayList<CompletedSet> sets = new ArrayList<>();

    private static ObservableList<BasketFx> basketFxObservableList = FXCollections.observableArrayList();

    public static void init() {
            Basket.basketFxObservableList = FXCollections.observableArrayList();
            Basket.pizzas.forEach(pizza -> {
                Basket.basketFxObservableList.add(ConverterToBasket.convertToBasketFx(pizza.getPrice(),pizza.getName(),pizza.getCode()));
            });
            Basket.drinks.forEach(drink -> {
                Basket.basketFxObservableList.add(ConverterToBasket.convertToBasketFx(drink.getPrice(),drink.getName(),drink.getCode()));
            });
            Basket.sets.forEach(set -> {
                Basket.basketFxObservableList.add(ConverterToBasket.convertToBasketFx(set.getPrice(),set.getName(),set.getCode()));
            });
            MyOrderController.reloadBasket();
    }
    public static ObservableList<BasketFx> getBasketFxObservableList(){return basketFxObservableList;}

    public static void addCompletedPizza(CompletedPizza cp) {
        pizzas.add(cp);
        init();
    }

    public static void addDrink(Drink cd) {
        drinks.add(cd);
        init();
    }

    public static void addSet(CompletedSet cs) {
        sets.add(cs);
        init();
    }

    public static void removePizza(CompletedPizza cp) {
        pizzas.remove(cp);
        init();
    }

    public static void removeDrink(Drink cd) {
        pizzas.remove(cd);
        init();
    }

    public static void removeSet(CompletedSet cs) {
        pizzas.remove(cs);
        init();
    }

    public static ArrayList<Drink> getDrinks(){ return drinks; }

    public static ArrayList<CompletedSet> getSets(){ return sets; }

    public static ArrayList<CompletedPizza> getPizzas(){
        return pizzas;
    }

    public static void clear(){
        pizzas = new ArrayList<>();
        drinks = new ArrayList<>();
        sets = new ArrayList<>();
        init();
    }



    @Override
    public String toString() {
        String result = "Basket{";
        for (CompletedPizza cp : pizzas) {
            result += "Pizza{" +
                    "name='" + cp.getName() + '\'' +
                    "desc='" + cp.getDesc() + '\'' +
                    "size='" + cp.getSize() + '\'' +
                    "code='" + cp.getCode() + '\'' +
                    "price='" + cp.getPrice() + '\'' +
                    "composition='" + cp.getComposition() + '\'' +
                    "}";
        }
        result += '}';
        return result;
    }
}