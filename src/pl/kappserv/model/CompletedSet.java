package pl.kappserv.model;

import java.util.ArrayList;

public class CompletedSet extends Product{
    private ArrayList<CompletedPizza> pizzas;
    private ArrayList<Drink> drinks;

    public CompletedSet(){
        this.pizzas = new ArrayList<>();
        this.drinks = new ArrayList<>();
    }

    public CompletedSet(ArrayList<CompletedPizza> cp, ArrayList<Drink> cd){
        this.pizzas = cp;
        this.drinks = cd;
    }

    public Integer getPrice(){
        Integer price = 0;
        for(CompletedPizza cp: pizzas){
            price += cp.getPrice();
        }

        for(Drink cd: drinks) {
            price += cd.getPrice();
        }

        return price;
    }

    public ArrayList<CompletedPizza> getPizzas(){
        return pizzas;
    }

    public void addPizza(CompletedPizza cp){
        this.pizzas.add(cp); }

    public ArrayList<Drink> getDrinks(){
        return drinks;
    }

    public void setDrinks(ArrayList<Drink> drinks){
        this.drinks = drinks;
    }
}
