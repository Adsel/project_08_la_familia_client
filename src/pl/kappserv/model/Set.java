package pl.kappserv.model;

import java.util.ArrayList;

public class Set extends Product{
    private ArrayList<Pizza> pizzas;
    private ArrayList<Drink> drinks;

    public Set(){ super(); }

    public Set(Integer id, Integer price, String name, String desc, String code, ArrayList<Pizza> pizzas, ArrayList<Drink> drinks){
        super(id, price, name, desc, code);
        this.pizzas = pizzas;
        this.drinks = drinks;
    }

    public ArrayList<Pizza> getPizzas(){ return this.pizzas; }

    public ArrayList<Drink> getDrinks(){ return this.drinks; }

}
