package pl.kappserv.model;

public class CompletedDrink extends Product {
    private String volume;

    public CompletedDrink(){
        super();
    }

    public CompletedDrink(Integer id, Integer price, String name, String desc, String code, String volume){
        super(id, price, name, desc, code);
        this.volume = volume;
    }

    public String getVolume(){
        return volume;
    }
}