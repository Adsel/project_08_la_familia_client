package pl.kappserv.gui;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.HashMap;

public class ScreenController {
    private static HashMap<String, Pane> screenMap = new HashMap<>();
    private Scene main;
    private Stage stage;

    public ScreenController(Scene main, Stage stage) {
        this.main = main;
        this.stage= stage;
    }

    public void addScreen(String name, Pane pane){
        screenMap.put(name, pane);
    }

    public void removeAllScreen(){
        screenMap.clear();
    }

    public void activate(String name){
        main.setRoot( screenMap.get(name) );
        this.stage.setMinHeight(720);
        this.stage.setMinWidth(1420);
        this.stage.setMaxHeight(720);
        this.stage.setMaxWidth(1420);
    }
    public void activate(String name, int x, int y){
        main.setRoot( screenMap.get(name) );
        this.stage.setHeight(y);
        this.stage.setWidth(x);
        this.stage.setMinHeight(y);
        this.stage.setMinWidth(x);
        this.stage.setMaxHeight(y);
        this.stage.setMaxWidth(x);

    }
}