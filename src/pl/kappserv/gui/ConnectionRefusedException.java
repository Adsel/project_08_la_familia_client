package pl.kappserv.gui;

public class ConnectionRefusedException extends Exception {
    public ConnectionRefusedException(){
        super();
    }

    public ConnectionRefusedException(String msg){
        super(msg);
    }
}
