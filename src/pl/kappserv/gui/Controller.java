package pl.kappserv.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import pl.kappserv.model.*;
import pl.kappserv.requests.Request;
import pl.kappserv.Main;

import java.util.ArrayList;

public abstract class Controller {
    public static String  getServerIp(){
        return "127.0.0.1:8080";
    }

    public static Request request = new Request();
    private static Integer tempId;
    private static boolean fromSet = false;
    private static ArrayList<Pizza> tempP;
    private static ArrayList<CompletedPizza> tempPizzas;
    private static ArrayList<Drink> tempD;
    private static Set tempS;

    @FXML protected MenuItem myMenu;
    @FXML protected MenuItem myAbout;
    @FXML protected MenuItem myOrder;
    @FXML protected Button myMainButton;
    @FXML protected Button myMenuButton;

    @FXML public void menuItemLoadView(ActionEvent event) {
        Main.activeScreen(((MenuItem)event.getSource()).getId());
    }

    public static Double convertFromGr(String value){
        Double gr = Double.parseDouble(value);
        if(gr == 0.0){
            return 0.0;
        }
        gr = gr / 100;
        return (Math.round(gr * 100.0) / 100.0);
    }

    public static String convertFromGrString(String value){
        Double temp = convertFromGr(value);
        return temp.toString();
    }

    public static void setTempId(Integer i){
        if(i > 0) {
            tempId = i;
        }
    }

    public static Integer getTempId(){
        return tempId;
    }

    public static boolean getFromSet(){ return fromSet; }

    public static void setFromSet(boolean f){ fromSet = f;}

    public static void setTempPizzas(){ tempPizzas = new ArrayList<>(); }


    public static void setTempP(ArrayList<Pizza> p){ tempP = p; }

    public static ArrayList<Pizza> getTempP(){ return tempP; }

    public static ArrayList<Drink> getTempD(){ return tempD; }

    public static Set getTempS(){ return tempS; }

    public static void setTempS(Set s){ tempS = s; }

    public static void setTempD(ArrayList<Drink> d){ tempD = d;}
}
