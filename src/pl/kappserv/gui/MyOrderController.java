package pl.kappserv.gui;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import pl.kappserv.Main;
import pl.kappserv.model.*;
import pl.kappserv.modelFx.BasketFx;
import pl.kappserv.modelFx.SetFx;
import pl.kappserv.requests.Request;
import pl.kappserv.utils.conventers.ConverterSet;

import java.io.IOException;
import java.util.ArrayList;

public class MyOrderController extends Controller{


    @FXML private VBox mainWindow;
    @FXML private MenuBar menuBar;
    @FXML private Menu informations;
    @FXML private Menu orders;
    @FXML private HBox hBoxOne;
    @FXML private VBox vBoxOne;
    @FXML private Text logo;
    @FXML private HBox hBoxTwo;
    @FXML private ImageView logoImage;
    @FXML private AnchorPane pane1;
    @FXML private VBox fourthBox;
    @FXML private Text footerText;
    @FXML private Button createReceipt;
    @FXML private Button deleteProduct;
    public static TableView orderTableStatic;
    @FXML private TableView orderTable;
    @FXML private TableColumn<BasketFx, String> orderName;
    public static TableColumn<BasketFx, String> orderNameStatic;
    @FXML private TableColumn<BasketFx, String> orderPrize;
    public static TableColumn<BasketFx, String> orderPrizeStatic;


    @FXML private TableColumn orderCount;

    @FXML private ArrayList<CompletedPizza> bucketPizza;
    void addBucketPizza(CompletedPizza cPizza){
        this.bucketPizza.add(cPizza);
    }
    void removeBucketPizza(CompletedPizza cPizza){
        this.bucketPizza.remove(cPizza);
    }

    public static void reloadBasket(){
        System.out.println(Basket.getBasketFxObservableList());
        orderTableStatic.setItems(Basket.getBasketFxObservableList());
        MyOrderController.orderNameStatic.setCellValueFactory(cellData -> cellData.getValue().getName());
        MyOrderController.orderPrizeStatic.setCellValueFactory(cellData -> cellData.getValue().getPrice());
    }

    @FXML
    void initialize(){
        orderTableStatic = orderTable;
        orderNameStatic = orderName;
        orderPrizeStatic = orderPrize;

        myMenu.setOnAction((event -> {
            Main.activeScreen("myMenu");
        }));

        myAbout.setOnAction((event -> {
            Main.activeScreen("myMain");
        }));

        myMainButton.setOnAction((event -> {
            Main.activeScreen("myMain");
        }));

        myOrder.setOnAction((event ->  {
            Main.activeScreen("myOrder");
        }));

        myMenuButton.setOnAction((event -> {
            Main.activeScreen("myMenu");
        }));

        // dla przycisku generuj paragon
        createReceipt.setOnAction((event ->{
            Receipt newReceipt = new Receipt(Basket.getDrinks(), Basket.getSets(), Basket.getPizzas());
            Request.createReceiptHTTPRequest(newReceipt);
            Basket.clear();
        }));


        deleteProduct.setOnAction((event ->{
            Basket.clear();
        }));

    }
}
