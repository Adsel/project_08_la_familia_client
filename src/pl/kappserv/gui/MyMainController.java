package pl.kappserv.gui;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import pl.kappserv.Main;

public class MyMainController extends Controller {
    @FXML private VBox main;
    @FXML private MenuBar menuBar;
    @FXML private Menu informations;
    @FXML private MenuItem myMenu;
    @FXML private MenuItem myAbout;
    @FXML private Menu orders;
    @FXML private MenuItem myOrder;
    @FXML private HBox hBoxOne;
    @FXML private FlowPane flowPane;
    @FXML private VBox vBoxOne;
    @FXML private ImageView imageLogo;
    @FXML private AnchorPane paneThree;
    @FXML private Button myMenuButton;
    @FXML private Button myOrderButton;
    @FXML private VBox vBoxTwo;
    @FXML private Text logo;
    @FXML private Region textField;
    @FXML private Text text1;
    @FXML private Text text2;
    @FXML private AnchorPane paneFour;
    @FXML private HBox hBoxTwo;
    @FXML private VBox vBoxThree;
    @FXML private Text hoursMain;
    @FXML private AnchorPane paneFive;
    @FXML private Text day1;
    @FXML private Text hour1;
    @FXML private Text day2;
    @FXML private Text hour2;
    @FXML private Text day3;
    @FXML private Text hour3;
    @FXML private VBox vBoxFour;
    @FXML private Text contact;
    @FXML private Text phoneMain;
    @FXML private Text phone1;
    @FXML private Text phone2;
    @FXML private Text addres;
    @FXML private Text street;
    @FXML private Text code;
    @FXML private VBox fourthBox;
    @FXML private Text footerText;

    @FXML
    void initialize() {
        myMenu.setOnAction((event -> {
            Main.activeScreen("myMenu");
        }));

        myMenuButton.setOnAction((event -> {
            Main.activeScreen("myMenu");
        }));

        myAbout.setOnAction((event -> {
            Main.activeScreen("myAbout");
        }));

        myOrder.setOnAction((event) -> {
            Main.activeScreen("myOrder");
        });

        myOrderButton.setOnAction((event) -> {
            Main.activeScreen("myOrder");
        });

        logo.setFont(Font.loadFont("file:src/pl/kappserv/fonts/EBGaramond-BoldItalic.ttf", 90));
    }

}
