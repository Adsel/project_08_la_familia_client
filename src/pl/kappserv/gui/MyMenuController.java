package pl.kappserv.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import pl.kappserv.Main;
import pl.kappserv.model.Basket;
import pl.kappserv.model.Pizza;
import pl.kappserv.model.Set;
import pl.kappserv.modelFx.*;
import pl.kappserv.utils.conventers.ConverterDrink;
import pl.kappserv.utils.conventers.ConverterSet;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class MyMenuController extends Controller implements Initializable {
    @FXML private VBox mainWindow;
    @FXML private MenuBar menuBar;
    @FXML private Menu informations;
    @FXML private Menu orders;
    @FXML private HBox firstBox;
    @FXML private VBox secondBox;
    @FXML private Text logo;
    @FXML private HBox thirdBox;
    @FXML private ImageView logoImage;
    @FXML private Button myMainButton;
    @FXML private Button myOrderButton;
    @FXML private TabPane tabPane;
    @FXML private Tab pizza;
    @FXML private AnchorPane paneOne;
    @FXML private HBox hBoxOne;
    @FXML private VBox vBoxOne;
    @FXML private AnchorPane paneTwo;
    @FXML private Circle circle1;
    @FXML private Circle circle2;
    @FXML private Circle circle3;
    @FXML private Text text1;
    @FXML private Text text2;
    @FXML private Text text3;
    @FXML private AnchorPane paneThree;
    @FXML private TableView<PizzaFx> productPizzaTable;
    @FXML private TableColumn<PizzaFx, String> productPizzaName;
    @FXML private TableColumn<PizzaFx, String> productPizzaComposition;
    @FXML private TableColumn<PizzaFx, String> productPizzaPriceL;
    @FXML private TableColumn<PizzaFx, String> productPizzaPriceXL;
    @FXML private TableColumn<PizzaFx, String> productPizzaPriceXXL;
    @FXML private Tab drinks;
    @FXML private AnchorPane paneFour;
    @FXML private TableView<DrinkFx> productDrinkTable;
    @FXML private TableColumn<DrinkFx, String> productDrinkType;
    @FXML private TableColumn<DrinkFx, String> productDrinkPrize;
    @FXML private TableColumn<DrinkFx, String> productDrinkDesc;
    @FXML private Tab other;
    @FXML private AnchorPane paneFive;
    @FXML private TableView<SetFx> productSetTable;
    @FXML private TableColumn<SetFx, String> productSetType;
    @FXML private TableColumn<SetFx, String> productSetPrize;
    @FXML private TableColumn<SetFx, String> productSetDesc;
    @FXML private VBox fourthBox;
    @FXML private Text footerText;
    @FXML private Text pizzaName;
    @FXML private Text pizzaPrice;
    @FXML private AnchorPane content;
    @FXML private ListPizzaModel listPizzaModel;
    @FXML private ListDrinkModel listDrinkModel;
    @FXML private ListSetModel listSetModel;
    @FXML private Button addDrinkBtn;
    @FXML private Button addSetBtn;

    public MyMenuController(){
        this.productPizzaTable = new TableView<>();
        this.productPizzaName = new TableColumn<>();
        this.productPizzaComposition = new TableColumn<>();
        this.productPizzaPriceL = new TableColumn<>();
        this.productPizzaPriceXL = new TableColumn<>();
        this.productPizzaPriceXXL = new TableColumn<>();
        this.pizzaName = new Text();
        this.pizzaPrice = new Text();
        this.productDrinkTable = new TableView<>();
        this.productDrinkType = new TableColumn<>();
        this.productDrinkPrize = new TableColumn<>();
        this.productDrinkDesc = new TableColumn<>();
    }

    private boolean change = false;
    @FXML private void handleClickTablePizzaView(MouseEvent click) throws IOException {
        String pizzaId = null;
        try{
            PizzaFx selectedPizza = productPizzaTable.getSelectionModel().getSelectedItem();
            String pizzaN = selectedPizza.getName().getValue();
            String pizzaP = selectedPizza.getPrice().getValue();
            String pizzaD = selectedPizza.getDesc().getValue();
            for(PizzaFx p: productPizzaTable.getItems()){
                if(p.getName().getValue().equals(pizzaN)){
                    pizzaId = p.getId().getValue();
                    change = true;
                    break;
                }
            }
            productPizzaTable.getSelectionModel().clearSelection();

            if(change){
                Controller.setTempId(Integer.parseInt(pizzaId));
                PizzaOptionsController.setPizzaName(pizzaN);
                PizzaOptionsController.setPizzaPrice(pizzaP);
                PizzaOptionsController.setPizzaDesc(pizzaD);
                Main.activeScreen("pizzaOptions");
            }
        } catch (Exception e){
            if(change!= true){
                System.out.println("Problem z funkcją Pizza Table View - prawdopodobnie pusta");
                Main.activeScreen("myMain");
            }
            else{
                System.out.println(e.getMessage());
            }
        }
    }

    private void getInfoAboutPizzas(Integer index){
        Pizza p = Controller.getTempP().get(index);
        String pizzaN = p.getName();
        String pizzaP = String.valueOf(p.getPrice());
        String pizzaD = p.getDesc();
        Controller.setFromSet(true);
        Controller.setTempId(p.getId());
        PizzaOptionsController.setPizzaName(pizzaN);
        PizzaOptionsController.setPizzaPrice(pizzaP);
        PizzaOptionsController.setPizzaDesc(pizzaD);
        Main.activeScreen("pizzaOptions");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        myMenu.setOnAction((event -> {
            Main.activeScreen("myMenu");
        }));

        myAbout.setOnAction((event -> {
            Main.activeScreen("myAbout");
        }));

        myMainButton.setOnAction((event -> {
            Main.activeScreen("myMain");
        }));

        myOrderButton.setOnAction((event -> {
            Main.activeScreen("myOrder");
        }));

        addSetBtn.setOnAction((event ->{
            SetFx selectedSetFx = productSetTable.getSelectionModel().getSelectedItem();

            if(selectedSetFx != null){
                Set selectedSet = ConverterSet.convertToSet(selectedSetFx);
                Integer idSet = selectedSet.getId();
                productSetTable.getSelectionModel().clearSelection();
                try {
                    Set fullInformationSet = Controller.request.getSetFromServer(idSet);
                    Controller.setTempPizzas();
                    Controller.setTempP(fullInformationSet.getPizzas());
                    getInfoAboutPizzas(0);
                    Controller.setTempD(fullInformationSet.getDrinks());
                    Controller.setTempS(fullInformationSet);
                    /*for(Pizza p: fullInformationSet.getPizzas()){

                    }*/
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }));

        addDrinkBtn.setOnAction((event ->{
            DrinkFx selectedDrink = productDrinkTable.getSelectionModel().getSelectedItem();
            if(selectedDrink != null){
                Basket.addDrink(ConverterDrink.convertToDrink(selectedDrink));
                productDrinkTable.getSelectionModel().clearSelection();
            }
        }));

        myOrder.setOnAction((event -> {
            Main.activeScreen("myOrder");}));
            this.listPizzaModel = new ListPizzaModel();
            this.listDrinkModel = new ListDrinkModel();
            this.listSetModel = new ListSetModel();

            try {
                this.listPizzaModel.init();
                this.listDrinkModel.init();
                this.listSetModel.init();

                this.productPizzaTable.setItems(this.listPizzaModel.getPizzaFxObservableList());
                this.productPizzaName.setCellValueFactory(cellData -> cellData.getValue().getName());
                this.productPizzaComposition.setCellValueFactory(cellData -> cellData.getValue().getComposition());
                this.productPizzaPriceXL.setCellValueFactory(cellData -> cellData.getValue().getPrice());

                this.productDrinkTable.setItems(this.listDrinkModel.getDrinkFxObservableList());
                this.productDrinkType.setCellValueFactory(cellData -> cellData.getValue().getName());
                this.productDrinkDesc.setCellValueFactory(cellData -> cellData.getValue().getDesc());
                this.productDrinkPrize.setCellValueFactory(cellData -> cellData.getValue().getPrice());
                //this.productSize.setCellValueFactory(cellData -> cellData.getValue().getSize().get(0));

                this.productSetTable.setItems(this.listSetModel.getSetFxObservableList());
                this.productSetType.setCellValueFactory(cellData -> cellData.getValue().getName());
                this.productSetDesc.setCellValueFactory(cellData -> cellData.getValue().getDesc());
                this.productSetPrize.setCellValueFactory(cellData -> cellData.getValue().getPrice());
            }
            catch (ConnectionRefusedException cre){
                Main.goNoConnection(cre.getMessage());
            }
            catch (Exception e){
                System.out.println("Problem z poborem danych do list.");
                System.out.println(e.getMessage());
                //Main.activeScreen("serverDisconnect");
            }
        }
    }