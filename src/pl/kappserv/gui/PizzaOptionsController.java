package pl.kappserv.gui;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import pl.kappserv.Main;
import pl.kappserv.model.*;
import pl.kappserv.modelFx.AdditionFx;
import pl.kappserv.modelFx.ListAdditionModel;
import pl.kappserv.modelFx.ListSauceModel;
import pl.kappserv.modelFx.SauceFx;
import pl.kappserv.utils.conventers.ConverterAddition;
import pl.kappserv.utils.conventers.ConverterSauce;

import java.io.IOException;
import java.util.ArrayList;


public class PizzaOptionsController {

    @FXML private AnchorPane paneOne;
    @FXML private Text text2;
    @FXML private Text type;
    @FXML private Text name;
    @FXML private Text price2;
    @FXML private Button save;
    @FXML private Button back;
    @FXML private RadioButton cm3;
    @FXML private RadioButton cm2;
    @FXML private RadioButton cm1;
    @FXML private RadioButton fiftyFifty;
    @FXML private Label pizzaName;
    public static Label pizzaNameK;
    @FXML private Label pizzaPrice;
    public static Label pizzaPriceK;
    @FXML private Label pizzaDesc;
    public static Label pizzaDescK;
    @FXML private TableView additionsTable;
    @FXML private TableColumn<AdditionFx, String> tabColAdditionName;
    @FXML private TableColumn<AdditionFx, CheckBoxListCell> tabColAdditionRemark;
    @FXML private TableColumn<SauceFx, String> tabColSauceName;
    @FXML private TableColumn<SauceFx, CheckBox> tabColSauceRemark;

    @FXML private TableView saucesTable;
    @FXML private TableColumn sauces;
    @FXML private ListAdditionModel listAdditionList;
    @FXML private ListSauceModel listSauceList;

    @FXML void initialize() {
        pizzaNameK = pizzaName;
        pizzaPriceK = pizzaPrice;
        pizzaDescK = pizzaDesc;

        this.listAdditionList = new ListAdditionModel();
        this.listSauceList = new ListSauceModel();

        try {
            this.listAdditionList.init();
            this.listSauceList.init();

            this.additionsTable.setItems(this.listAdditionList.getAdditionFxObservableList());
            this.tabColAdditionName.setCellValueFactory(cellData -> cellData.getValue().getName());
            this.additionsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            this.saucesTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

            this.saucesTable.setItems(this.listSauceList.getSauceFxObservableList());
            this.tabColSauceName.setCellValueFactory(cellData -> cellData.getValue().getName());

        } catch (ConnectionRefusedException cre){
                Main.goNoConnection(cre.getMessage());
        }

        back.setOnAction((event ->  {
            clearSetting();
            Main.activeScreen("myMenu");
        }));

        save.setOnAction((event -> {
            try {
                Pizza pf = Controller.request.getPizzaFromServer(Controller.getTempId());
                ArrayList<Addition> additions = new ArrayList<>();
                for(int i = 0; i < additionsTable.getSelectionModel().getSelectedItems().size(); i++){
                    AdditionFx selectedAddition = (AdditionFx) additionsTable.getSelectionModel().getSelectedItems().get(i);
                    additions.add(ConverterAddition.convertToAddition(selectedAddition));
                }

                ArrayList<Sauce> sauces = new ArrayList<>();
                ObservableList saucesList = saucesTable.getSelectionModel().getSelectedItems();
                for(int i = 0; i < saucesList.size(); i++){
                    SauceFx selectedSauce = (SauceFx) saucesTable.getSelectionModel().getSelectedItems().get(i);
                    sauces.add(ConverterSauce.convertToSauce(selectedSauce));
                }

                Boolean wh = true;
                if(fiftyFifty.isSelected()){
                   wh = false;
                }

                CompletedPizza cp = new CompletedPizza(pf.getId(), pf.getPrice(), pf.getName(), pf.getDesc(), pf.getCode(), pf.getSize(), pf.getComposition(), additions, sauces, wh);
                if(!Controller.getFromSet()) {
                    Basket.addCompletedPizza(cp);
                    System.out.println("Dodano do koszyka:");
                    System.out.println(cp);
                }
                else{
                    System.out.println("Dodano do koszyka zestaw:");
                    System.out.println(cp);


                    // DODANIE KOMPLETNEGO ZESTAWU (Z SOSAMI)
                    CompletedSet cs = new CompletedSet();
                    cs.setId(Controller.getTempS().getId());
                    cs.setName(Controller.getTempS().getName());
                    cs.setCode(Controller.getTempS().getCode());
                    cs.setPrice(Controller.getTempS().getPrice());
                    for(Pizza pz : Controller.getTempS().getPizzas()){
                        CompletedPizza tempP = new CompletedPizza(pz.getId(), cp.getPrice(), cp.getName(), cp.getDesc(), cp.getCode(), cp.getSize(), cp.getComposition(), additions, sauces, true);
                            cs.addPizza(tempP);
                    }
                    cs.setDrinks(Controller.getTempD());
                    Basket.addSet(cs);

                    //czyszczenie
                    Controller.setTempP(new ArrayList<>());
                    Controller.setFromSet(false);
                    Controller.setTempD(new ArrayList<>());
                    Controller.setTempS(null);
                }
            } catch (IOException e) {
                System.out.println("Nie udalo sie pobrac informacji o pizzy o ID " + Controller.getTempId());
            }

            clearSetting();
            Main.activeScreen("myMenu");
        }));
    }

    public static void setPizzaName(String text){
        pizzaNameK.setText(text);
    }

    private void clearSetting(){
        if(fiftyFifty.isSelected()){
            fiftyFifty.setSelected(false);
        }

        saucesTable.getSelectionModel().clearSelection();
        additionsTable.getSelectionModel().clearSelection();
    }

    public static void setPizzaPrice(String text){
        pizzaPriceK.setText(text + " zł");
    }

    public static void setPizzaDesc(String text){ pizzaDescK.setText(text); }
}
