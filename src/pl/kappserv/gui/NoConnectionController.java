package pl.kappserv.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import pl.kappserv.Main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class NoConnectionController extends Controller implements Initializable {

    @FXML Button again;
    @FXML AnchorPane PaneWindow;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        again.setOnAction((event -> {
            try {
                Main.reloadScreen();
                Main.activeScreen("myMain");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
    }

}
